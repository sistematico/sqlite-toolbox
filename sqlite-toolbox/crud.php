<?php


    public function create($tabela)
    {
        $sql = "CREATE TABLE IF NOT EXISTS $tabela (id INTEGER PRIMARY KEY, nome TEXT, sobrenome TEXT, email TEXT, cadastro INTEGER, validado BOOLEAN)";

        try {
            $this->db->exec($sql);
            return "Tabela $tabela criada com sucesso.";
        } catch (\PDOException $e) {
            return "Erro ao criar tabela $tabela: " . $e->getMessage();
        }
    }

    public function read($tabela)
    {
    	$sql = "SELECT id,nome,sobrenome,email,cadastro,validado FROM $tabela";

        try {
            $query = $this->db->prepare($sql);
            $query->execute();
            return $query->fetchAll();   
        } catch (\PDOException $e) {
            return "Erro ao ler tabela $tabela: " . $e->getMessage();
        }
    }

    public function update($tabela,$id,$nome,$sobrenome,$email,$cadastro,$validado) 
    {

        $sql = "UPDATE $tabela SET nome = :nome, sobrenome = :sobrenome, email = :email, validado = :validado WHERE id = :id";

        try {

        	$query = $this->db->prepare($sql);

        	$query->bindParam(':id', $id);
        	$query->bindParam(':nome', $nome);
        	$query->bindParam(':nome', $sobrenome);
        	$query->bindParam(':email', $email);
        	$query->bindParam(':role', $role);
        	$query->bindParam(':validado', $validado);

        	$query->execute();        	

		} catch (\PDOException $e) {
            return "Erro ao ler tabela $tabela: " . $e->getMessage();
        }
    }

    public function delete($tabela,$id) 
    {
        try {

            $sql = "DELETE FROM $tabela WHERE id = :id";
            
            $query = $this->db->prepare($sql);

            $query->bindParam(':id', $id);
            
            $query->execute();
            
        } catch (\PDOException $e) { 
            return "Erro ao apagar id $id: " . $e->getMessage();
        }
    }